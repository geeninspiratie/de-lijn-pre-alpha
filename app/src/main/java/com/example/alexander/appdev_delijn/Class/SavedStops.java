package com.example.alexander.appdev_delijn.Class;

/**
 * Created by Alexander on 07-Nov-17.
 */

public class SavedStops {
    private String halteNr, beschrijving;

    public SavedStops(String halteNr, String beschrijving) {
        this.halteNr = halteNr;
        this.beschrijving = beschrijving;
    }

    public String getHalteNr() {
        return halteNr.toString();
    }

    public String getBeschrijving() {
        return beschrijving.toString();
    }

    public String toString() {
        return halteNr + ":" + beschrijving;
    }
}
