package com.example.alexander.appdev_delijn.API;
import com.example.alexander.appdev_delijn.Class.BusInformation;
import com.loopj.android.http.*;

import java.util.List;

/**
 * Created by Alexander on 02-Nov-17.
 */

public class DeLijnRestClient {
    private static final String BASE_URL = "https://www.delijn.be/rise-api-core/";
    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
