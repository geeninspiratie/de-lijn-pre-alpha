package com.example.alexander.appdev_delijn.Class;

/**
 * Created by Alexander on 02-Nov-17.
 */

public class BusInformation {
    private String bestemming;
    private String kleurAchtergrond;
    private String kleurAchtergrondRand;

    private String kleurVoorgrond;

    private String kleurVoorgrondRand;
    private int lijnNummer;
    private String lijnNummerPubliek;
    private String vertrekTijd;

    public BusInformation(String bestemming, String kleurAchtergrond, String kleurAchtergrondRand, String kleurVoorgrond, String kleurVoorgrondRand, int lijnNummer, String lijnNummerPubliek, String vertrekTijd) {
        this.bestemming = bestemming;
        this.kleurAchtergrond = kleurAchtergrond;
        this.kleurAchtergrondRand = kleurAchtergrondRand;
        this.kleurVoorgrond = kleurVoorgrond;
        this.kleurVoorgrondRand = kleurVoorgrondRand;
        this.lijnNummer = lijnNummer;
        this.lijnNummerPubliek = lijnNummerPubliek;
        this.vertrekTijd = vertrekTijd;
    }

    public String toStringBestemming() {
        return this.lijnNummerPubliek + " " + this.bestemming;
    }

    public String toStringTijd() {
        return "Doorkomst: " + this.vertrekTijd;
    }

    public String getBestemming() {
        return bestemming.toString();
    }

    public String getKleurAchtergrond() {
        return kleurAchtergrond.toString();
    }

    public String getLijnNummerPubliek() {
        return lijnNummerPubliek.toString();
    }

    public String getVertrekTijd() {
        return vertrekTijd.toString();
    }
}
