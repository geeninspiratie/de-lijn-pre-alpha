package com.example.alexander.appdev_delijn.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.alexander.appdev_delijn.Class.SavedStops;
import com.example.alexander.appdev_delijn.R;

import java.util.List;

/**
 * Created by Alexander on 10-Nov-17.
 */

public class SavedStopsAdapter extends BaseAdapter {
    private Activity activity;
    private List<SavedStops> savedStopsList;

    public SavedStopsAdapter(Activity activity, List<SavedStops> savedStopsList) {
        this.activity = activity;
        this.savedStopsList = savedStopsList;
    }

    private class ViewHolder {
        private TextView tvHalteNaam;
        private TextView tvHalteNummer;
    }

    @Override
    public int getCount() {
        return savedStopsList.size();
    }

    @Override
    public Object getItem(int i) {
        return savedStopsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;


        view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_savedstops_custom, viewGroup, false);
        holder = new ViewHolder();
        holder.tvHalteNaam = (TextView) view.findViewById(R.id.tv_halteNaam);
        holder.tvHalteNummer = (TextView) view.findViewById(R.id.tv_halteNummer);
        view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_savedstops_custom, viewGroup, false);
        holder = new ViewHolder();
        holder.tvHalteNaam = (TextView) view.findViewById(R.id.tv_halteNaam);
        holder.tvHalteNummer = (TextView) view.findViewById(R.id.tv_halteNummer);


        holder.tvHalteNummer.setText(savedStopsList.get(i).getHalteNr());
        holder.tvHalteNaam.setText(savedStopsList.get(i).getBeschrijving());

        System.out.println("**-*-*-*-*-*--*-**Added" + savedStopsList.get(i).toString());
        return view;
    }

    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }
}
