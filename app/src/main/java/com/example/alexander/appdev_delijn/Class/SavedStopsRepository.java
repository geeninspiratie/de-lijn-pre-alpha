package com.example.alexander.appdev_delijn.Class;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Michiel on 9/11/2017.
 */

public class SavedStopsRepository {
    private DBHelper dbHelper;

    public SavedStopsRepository(Context context){
        dbHelper = new DBHelper(context);
    }

    public void insert(SavedStops stops) {

        //Open connection to write data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("halte", stops.getHalteNr());
        values.put("beschrijving",stops.getBeschrijving());

        // Inserting Row
        long student_Id = db.insert("delijn", null, values);
        db.close(); // Closing database connection
    }

    public ArrayList<HashMap<String, String>> getSavedStopsList() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  halte, beschrijving FROM delijn";

        ArrayList<HashMap<String, String>> stopsList = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> savedStops = new HashMap<String, String>();
                savedStops.put("halte", cursor.getString(cursor.getColumnIndex("halte")));
                savedStops.put("beschrijving", cursor.getString(cursor.getColumnIndex("beschrijving")));
                stopsList.add(savedStops);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return stopsList;

    }
}
