package com.example.alexander.appdev_delijn.Class;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Michiel on 9/11/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_NAME = "delijn.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_BUSINFORMATION = "CREATE TABLE delijn (id INTEGER PRIMARY KEY AUTOINCREMENT, halte TEXT, beschrijving TEXT)";

        db.execSQL(CREATE_TABLE_BUSINFORMATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS delijn");
        onCreate(db);
    }
}
