package com.example.alexander.appdev_delijn.View;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;

import com.example.alexander.appdev_delijn.API.DeLijnRestClient;
import com.example.alexander.appdev_delijn.Adapter.BusInformationAdapter;
import com.example.alexander.appdev_delijn.Class.ApiCall;
import com.example.alexander.appdev_delijn.Class.BusInformation;
import com.example.alexander.appdev_delijn.Class.SavedStops;
import com.example.alexander.appdev_delijn.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ShowResults extends AppCompatActivity {


    protected List<BusInformation> busInfo = new ArrayList<BusInformation>();
    protected String halteNr;
    protected ListView lstResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);
        lstResult = (ListView) findViewById(R.id.lstResult);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        BusInformationAdapter listViewAdapter = null;
        halteNr = getIntent().getExtras().getString("halteNr");


        //Make API Call
        ApiCall apiCall = (ApiCall) new ApiCall(halteNr);

        CallApi(this); /** Ophalen van DeLijn Rest informatie van meegegeven halte (private halteNr) */

        //Opslaan van gegevens via FAB
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ShowResults.this);
                alertBuilder.setTitle("Halte bewaren");

                final EditText input = new EditText(ShowResults.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setHint("Halte -> Bestemming");
                input.setMaxLines(1);
                alertBuilder.setView(input);

                alertBuilder.setPositiveButton("Bewaren", new DialogInterface.OnClickListener() {
                    //                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //halte wegschrijven naar bestand
                            writeToFile(halteNr + ":" + input.getText().toString(), ShowResults.this);
                    }
                });

                alertBuilder.setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog dialog = alertBuilder.create();
                //dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE); //automatisch openen van Keyboard
                dialog.show();
            }
        });

        //onRefresh; Api opnieuw opvragen en ListView Updaten
        final Activity ShowResults = this;
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        CallApi(ShowResults);
                        if (mySwipeRefreshLayout.isRefreshing()) {
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }
        );
    }

    private void OnClickListener() {

    }

    //Methode om opgeslagenHalte weg te schrijven naar opgeslagenHaltes.txt
    private void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("opgeslagenHaltes.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);
            outputStreamWriter.write("\n");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    /** Haltes opvragen via asyncHTTP request |-> loopj */
    public void CallApi(final Activity activity) {
        //Leegmaken van ListArray tervoorkoming van duplicaten
        if (busInfo.size() != 0) {
            busInfo.clear();
        }
        DeLijnRestClient.get("haltes/vertrekken/" + halteNr + "/10?New%20item=", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                Log.d("Response", "server response : " + response);
                try {
                    JSONArray arr = response.getJSONArray("lijnen");

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject serverResp = arr.getJSONObject(i);

                        String bestemming = serverResp.getString("bestemming");
                        String kleurAchtergrond = serverResp.getString("kleurAchterGrond");
                        String kleurAchtergrondRand = serverResp.getString("kleurAchterGrondRand");
                        String kleurVoorgrond = serverResp.getString("kleurVoorGrond");
                        String kleurVoorgrondRand = serverResp.getString("kleurVoorGrondRand");
                        int lijnNummer = serverResp.getInt("lijnNummer");
                        String lijnNummerPubliek = serverResp.getString("lijnNummerPubliek");
                        String vertrekTijd = serverResp.getString("vertrekTijd");

                        BusInformation bus = new BusInformation(bestemming, kleurAchtergrond, kleurAchtergrondRand, kleurVoorgrond, kleurVoorgrondRand, lijnNummer, lijnNummerPubliek, vertrekTijd);
                        System.out.println("OUTPUT-------------------" + bus.toStringBestemming() + " " + bus.toStringTijd());
                        busInfo.add(bus);
                    }

                    //List inladen in ListView in ShowResults Activity.
                    BusInformationAdapter listViewAdapter = new BusInformationAdapter(activity, busInfo);

                    lstResult.setAdapter(listViewAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }
}
