package com.example.alexander.appdev_delijn.Class;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceActivity;
import android.util.Log;

import com.example.alexander.appdev_delijn.API.DeLijnRestClient;

import org.json.*;

import com.loopj.android.http.*;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Alexander on 24-Oct-17.
 */

public class ApiCall extends AsyncTask<Void, Void, List<BusInformation>> {
    private final List<BusInformation> busList = new ArrayList<>();
    private boolean done = false;
    private String halteNr;

    public ApiCall(String HalteNr) {
        this.halteNr = HalteNr;
    }

    public List<BusInformation> getBusListAfterAPI() {
        return busList;
    }

    public void makeCall(String halteNr) throws JSONException {
        final List<BusInformation> busList = new ArrayList<>();
        DeLijnRestClient.get("haltes/vertrekken/" + halteNr + "/25?New%20item=", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                Log.d("Response", "server response : " + response);
                try {
                    JSONArray arr = response.getJSONArray("lijnen");

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject serverResp = arr.getJSONObject(i);

                        String bestemming = serverResp.getString("bestemming");
                        String kleurAchtergrond = serverResp.getString("kleurAchterGrond");
                        String kleurAchtergrondRand = serverResp.getString("kleurAchterGrondRand");
                        String kleurVoorgrond = serverResp.getString("kleurVoorGrond");
                        String kleurVoorgrondRand = serverResp.getString("kleurVoorGrondRand");
                        int lijnNummer = serverResp.getInt("lijnNummer");
                        String lijnNummerPubliek = serverResp.getString("lijnNummerPubliek");
                        String vertrekTijd = serverResp.getString("vertrekTijd");

                        BusInformation bus = new BusInformation(bestemming, kleurAchtergrond, kleurAchtergrondRand, kleurVoorgrond, kleurVoorgrondRand, lijnNummer, lijnNummerPubliek, vertrekTijd);
                        System.out.println("OUTPUT-------------------" + bus.toStringBestemming() + " " + bus.toStringTijd());
                        busList.add(bus);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    JSONArray serverResp = new JSONArray(response.toString());
                    System.out.println(serverResp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    @Override
    protected List<BusInformation> doInBackground(Void... voids) {

        Handler mainHandler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {


            }
        };
        mainHandler.post(myRunnable);

        return busList;
    }
}


