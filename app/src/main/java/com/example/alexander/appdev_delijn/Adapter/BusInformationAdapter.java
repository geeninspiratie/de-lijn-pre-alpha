package com.example.alexander.appdev_delijn.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.alexander.appdev_delijn.Class.BusInformation;
import com.example.alexander.appdev_delijn.R;

import java.util.List;

/**
 * Created by Alexander on 03-Nov-17.
 */

public class BusInformationAdapter extends BaseAdapter {
    private Activity context;
    private List<BusInformation> busInfo;
    protected TextView tvBestemming;
    protected TextView tvVertrekTijd;
    protected TextView tvLijnNummer;

    public BusInformationAdapter(Activity context, List<BusInformation> busInfo) {
        this.context = context;
        this.busInfo = busInfo;
    }

    private class ViewHolder {
        public TextView tvBestemming;
        protected TextView tvVertrekTijd;
        protected TextView tvLijnNummer;
    }

    @Override
    public int getCount() {
        return busInfo.size();
    }

    @Override
    public Object getItem(int i) {
        return busInfo.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_custom, parent, false);
            holder = new ViewHolder();
            holder.tvBestemming = (TextView) view.findViewById(R.id.tv_bestemming);
            holder.tvVertrekTijd = (TextView) view.findViewById(R.id.tv_vertrektijd);
            holder.tvLijnNummer = (TextView) view.findViewById(R.id.tv_lijnnummer);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tvBestemming.setText(this.busInfo.get(i).getBestemming());
        holder.tvLijnNummer.setText(busInfo.get(i).getLijnNummerPubliek());
        holder.tvLijnNummer.setTextColor(Color.parseColor(busInfo.get(i).getKleurAchtergrond()));
        holder.tvVertrekTijd.setText(busInfo.get(i).getVertrekTijd());

        System.out.println("**-*-*-*-*-*--*-**Added " + busInfo.get(i).toStringBestemming() + " " + busInfo.get(i).toStringTijd());
        return view;
    }

    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }
}
