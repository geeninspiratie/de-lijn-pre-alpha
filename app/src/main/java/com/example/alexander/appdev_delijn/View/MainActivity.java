package com.example.alexander.appdev_delijn.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alexander.appdev_delijn.Adapter.SavedStopsAdapter;
import com.example.alexander.appdev_delijn.Class.BusInformation;
import com.example.alexander.appdev_delijn.Class.SavedStops;
import com.example.alexander.appdev_delijn.Class.SignInActivity;
import com.example.alexander.appdev_delijn.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button mButton;
    EditText mText;
    private List<SavedStops> opgeslagenHaltes = new ArrayList<>();
    private File file;
    private ListView lstStops;
    private boolean loggedIn = false;
    //ListView A

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        lstStops = (ListView) findViewById(R.id.lstStops);

        setSupportActionBar(toolbar);

        /** Inlezen van opgeslagenHaltes.txt en ListView opvullen */
        try {
            inlezenOpgeslagenHaltes();

        } catch (IOException e) {
            e.printStackTrace();
        }

        /** naar onderSwipen -> Herinlezen van opgeslagenHaltes.txt en Listview updaten */
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            inlezenOpgeslagenHaltes();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (mySwipeRefreshLayout.isRefreshing()) {
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }
        );

        mButton = (Button) findViewById(R.id.btnShow);
        mText = (EditText) findViewById(R.id.txtNumberStop);

        /** button "DoorKomsten" |-> opent nieuwe view ("ShowResults") en geeft parameter mee */
        mButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        String halteNr = mText.getText().toString();

                        Snackbar snackbar = Snackbar
                                .make(view, halteNr, Snackbar.LENGTH_LONG);

                        snackbar.show();

                        Intent intent = new Intent(MainActivity.this, ShowResults.class);
                        intent.putExtra("halteNr", halteNr);
                        startActivity(intent);

                    }
                });

        /** OnClickListener voor ListView |-> Vraagt HalteNummer op en geeft ze mee naar "ShowResults" view */
        lstStops.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ShowResults.class);
                intent.putExtra("halteNr", opgeslagenHaltes.get(position).getHalteNr());
                startActivity(intent);
            }
        });
    }

    //OnClick three-dot Menu Item (Login) | Opent SignInActivity voor authenticatie
    public void Login(MenuItem item) {
        Intent intent = new Intent(MainActivity.this, SignInActivity.class);
        startActivity(intent);
    }

    public void Update(MenuItem item) throws IOException {
        inlezenOpgeslagenHaltes();
    }


    /** Inlezen van txt bestand 'opgeslagenHaltes.txt' en deze verwerken in een ArrayList voor Toekomstig gebruik */
    public void inlezenOpgeslagenHaltes() throws IOException {
        FileInputStream fis;

        opgeslagenHaltes.clear(); /** Leegmaken van ArrayList en ListView */
        lstStops.setAdapter(null);

        file = new File(this.getExternalFilesDir(null), "opgeslagenHaltes.txt");
        if (!file.exists()) file.createNewFile();
        SavedStops savedStops;
        fis = openFileInput(file.getName());

        BufferedReader bf = new BufferedReader(new InputStreamReader(fis));
        String line = bf.readLine();
        while (line != null) {
            String[] arr = line.split(":");
            savedStops = new SavedStops(arr[0], arr[1]); //halteNr, beschrijving
            opgeslagenHaltes.add(savedStops);
            line = bf.readLine();
        }
        bf.close();
        fis.close();

        //ListView updaten;
        SavedStopsAdapter opgeslagenHaltesAdapter = new SavedStopsAdapter(this, opgeslagenHaltes);
        lstStops.setAdapter(opgeslagenHaltesAdapter);
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
